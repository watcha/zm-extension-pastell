/*

Copyright (C) 2016-2020  Barry de Graaff

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.

*/

package fr.starxpert.pastell;

import com.zimbra.client.ZMailbox;
import com.zimbra.client.ZMessage;
import com.zimbra.common.auth.ZAuthToken;
import com.zimbra.common.service.ServiceException;
import com.zimbra.common.util.ZimbraLog;
import com.zimbra.cs.account.soap.SoapProvisioning;
import com.zimbra.cs.db.*;
import com.zimbra.cs.extension.ExtensionHttpHandler;
import com.zimbra.cs.account.Provisioning;
import com.zimbra.cs.account.Account;
import com.zimbra.cs.account.AuthToken;
import com.zimbra.cs.account.Cos;

import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.zimbra.cs.mailbox.*;
import com.zimbra.cs.session.PendingModifications;
import com.zimbra.soap.type.AccountBy;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.zimbra.cs.mailbox.Flag.BITMASK_UNCACHED;

public class PastellAPI extends ExtensionHttpHandler {

    /**
     * The path under which the handler is registered for an extension.
     * return "/mytest" makes it show up under:
     * https://testserver.example.com/service/extension/mytest
     * @return path
     */
    @Override
    public String getPath() {
        return "/pastell";
    }
    /**
     * Processes HTTP GET requests.
     *
     * @param req  request message
     * @param resp response message
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        //Set the content type for the response.
        resp.setHeader("Content-Type", "text/plain;charset=UTF-8");
        //Do the response to the client.
        resp.getOutputStream().print("OK");
    }

    /**
     * Processes HTTP POST requests.
     *
     * @param req  request message
     * @param resp response message
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        //Read cookies and look for the ZM_AUTH_TOKEN cookie.
        String authTokenStr = null;

        Cookie[] cookies = req.getCookies();
        Cookie cookie = null;
        for (int n = 0; n < cookies.length; n++) {
            Cookie tmpCookie = cookies[n];

            if (tmpCookie.getName().equals("ZM_AUTH_TOKEN")) {
                authTokenStr = tmpCookie.getValue();
                cookie = tmpCookie;
                break;
            }
        }

        //Validate active user by parsing Zimbra AuthToken and read a cos value to see if its a valid user.
        Account account = null;

        if (authTokenStr != null) {
            try {
                AuthToken authToken = AuthToken.getAuthToken(authTokenStr);
                Provisioning prov = Provisioning.getInstance();
                account = Provisioning.getInstance().getAccountById(authToken.getAccountId());
                Cos cos = prov.getCOS(account);

                Set<String> allowedDomains = cos.getMultiAttrSet(Provisioning.A_zimbraProxyAllowedDomains);
                //In case no exception was thrown, it is a valid user.
            } catch (Exception ex) {
                //This was not a valid authtoken.
                ex.printStackTrace();
                return;
            }
        } else {
            //There was no authtoken found.
            return;
        }

        try {
            //Get the jsondata field from the multipart request send to the server and parse it to JSON Object.
            JSONObject receivedJSON = new JSONObject(IOUtils.toString(req.getPart("jsondata").getInputStream(), "UTF-8"));
            String action = receivedJSON.getString("action");
            String server = receivedJSON.getString("server");
            String baseUrl = receivedJSON.getString("zimbraserver");
            String auth = receivedJSON.getString("auth");
            String rspMsg = "";

            if(action.equals("login")) {
                rspMsg = loginPastell(server, auth);
            }
            if(action.equals("sendmsg")) {
                String text = receivedJSON.getString("text");
                String subject = receivedJSON.getString("subject");
                String to = receivedJSON.getString("to");
                String cc = receivedJSON.getString("cc");
                String bcc = receivedJSON.getString("bcc");
                int msgId = Integer.parseInt(receivedJSON.getString("msgid"));
                JSONObject filesObject = new JSONObject(receivedJSON.getString("files"));

                String dataForPastell = "";
                dataForPastell += "to=" + URLEncoder.encode(to, StandardCharsets.UTF_8) + "&";
                dataForPastell += "cc=" + URLEncoder.encode(cc, StandardCharsets.UTF_8) + "&";
                dataForPastell += "bcc=" + URLEncoder.encode(bcc, StandardCharsets.UTF_8) + "&";
                dataForPastell += "objet=" + URLEncoder.encode(subject, StandardCharsets.UTF_8) + "&";
                dataForPastell += "message=" + URLEncoder.encode(text, StandardCharsets.UTF_8);

                //get the user entity from the pastell server
                String entity = getPastellEntity(server, auth);
                if (!entity.equals("error")) {
                    //Create the secure message
                    String documentId = createDocument(server, auth, entity);
                    if (!documentId.equals("error")) {
                        //Add the data to the message
                        String documentEdit = editDocument(server, auth, entity, documentId, dataForPastell);
                        if (!documentEdit.equals("error")) {
                            //String baseUrl = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort();
                            boolean uploadSuccess = true;
                            if(filesObject.names() != null) {
                                for (int i = 0; i < filesObject.names().length(); i++) {
                                    String key = filesObject.names().getString(i);
                                    String fileUrl = filesObject.getString(key);
                                    String fileUpload = uploadFile(server, auth, entity, documentId, baseUrl + fileUrl + "&zauthtoken=" + authTokenStr, key, i, cookie);
                                    if (!fileUpload.equals("error")) {
                                        rspMsg = "{'error' : 'success'}";
                                    } else {
                                        uploadSuccess = false;
                                        rspMsg = "{'error' : 'error occurred while uploading files'}";
                                    }
                                }
                            }
                            if (uploadSuccess) {
                                //Send the message with pastell
                                rspMsg = sendDocument(server, auth, entity, documentId, msgId, account);
                            }
                        } else {
                            rspMsg = "{'error' : 'error occurred while editing message'}";
                        }
                    } else {
                        rspMsg = "{'error' : 'error occurred while creating message'}";
                    }
                } else {
                    rspMsg = "{'error' : 'error occurred while getting user entity'}";
                }
            }

            //Set the content type for the response.
            resp.setHeader("Content-Type", "text/json;charset=UTF-8");
            //Do the response to the client.
            resp.getOutputStream().print(rspMsg);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Login in pastell
    public static String loginPastell(String server, String auth) throws IOException, JSONException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        String rspMsg = "";
        HttpGet request = new HttpGet(server+"version");
        request.addHeader("Authorization", auth);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        rspMsg = ""+statusCode;
        return rspMsg;
    }

    //Create message in pastell
    public static String getPastellEntity(String server, String auth) throws IOException, JSONException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        String rspMsg = "";
        HttpGet request = new HttpGet(server+"entite");
        request.addHeader("Authorization", auth);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        if(statusCode == HttpStatus.SC_OK) {
            String json = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            JSONArray o = new JSONArray(json);
            rspMsg = o.getJSONObject(0).getString("id_e");
        }
        else {
            rspMsg = "error";
        }
        return rspMsg;
    }

    //Create message in pastell
    public static String createDocument(String server, String auth, String entity) throws IOException, JSONException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        String rspMsg = "";
        HttpPost request = new HttpPost(server+"entite/"+entity+"/document");
        StringEntity params = new StringEntity("type=mailsec-bidir");
        request.addHeader("Authorization", auth);
        request.addHeader("content-type", "application/x-www-form-urlencoded");
        request.setEntity(params);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        if(statusCode == HttpStatus.SC_CREATED) {
            String json = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            JSONObject o = new JSONObject(json);
            rspMsg = o.getJSONObject("info").getString("id_d");
        }
        else {
            rspMsg = "error";
        }
        return rspMsg;
    }

    //Add datas to the message in pastell
    public static String editDocument(String server, String auth, String entity, String documentId,String dataForPastell) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        String rspMsg = "";
        HttpPatch request = new HttpPatch(server+"entite/"+entity+"/document/"+documentId);
        StringEntity params = new StringEntity(dataForPastell);
        request.addHeader("Authorization", auth);
        request.addHeader("content-type", "application/x-www-form-urlencoded");
        request.setEntity(params);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        if(statusCode == HttpStatus.SC_OK) {
            rspMsg = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
        }
        else {
            rspMsg = "error";
        }
        return rspMsg;
    }

    //Add file to the message in pastell
    public static String uploadFile(String server, String auth, String entity, String documentId, String fileUrl, String filename, int index, Cookie cookie) {
        String rspMsg = "";
        try {
        BasicCookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie bcookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
        bcookie.setDomain(cookie.getDomain());
        bcookie.setPath(cookie.getPath());
        cookieStore.addCookie(bcookie);
        SSLContextBuilder sslBuilder = new SSLContextBuilder();
        sslBuilder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslBuilder.build());
        CloseableHttpClient localHttpClient = HttpClients.custom().setSSLSocketFactory(sslsf).setDefaultCookieStore(cookieStore).build();
        HttpGet getRequest = new HttpGet(fileUrl);

        HttpResponse getResponse = localHttpClient.execute(getRequest);

        int getStatusCode = getResponse.getStatusLine().getStatusCode();
        if(getStatusCode == HttpStatus.SC_OK) {
            Header ct = getResponse.getEntity().getContentType();
            InputStream is = getResponse.getEntity().getContent();

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(server+"entite/"+entity+"/document/"+documentId+"/file/document_attache/"+index);
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addTextBody("file_name", filename);
            builder.addBinaryBody("file_content", is, ContentType.parse(ct.getValue()), filename);
            HttpEntity fileEntity = builder.build();

            request.addHeader("Authorization", auth);
            request.setEntity(fileEntity);
            HttpResponse response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == HttpStatus.SC_CREATED) {
                rspMsg = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            }
            else {
                rspMsg = "error";
            }
        }
        else {
            rspMsg = "error";
        }
        } catch (Exception e) {
            ZimbraLog.webclient.error("Erreur pastell", e.fillInStackTrace());
        }
        return rspMsg;
    }

    //Send message with pastell
    public static String sendDocument(String server, String auth, String entity, String documentId, int msgId, Account account) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        String rspMsg = "";
        HttpPost request = new HttpPost(server+"entite/"+entity+"/document/"+documentId+"/action/envoi-mail");
        request.addHeader("Authorization", auth);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        if(statusCode == HttpStatus.SC_CREATED) {
            rspMsg = "{'error' : 'success'}";
            removeDraftStatus(msgId, account);
        }
        else {
            rspMsg = "{'error' : 'error occurred while sending message'}";
        }
        return rspMsg;
    }

    //Helper method to combine JSON objects
    public static JSONObject mergeJSONObjects(JSONObject json1, JSONObject json2) {
        JSONObject mergedJSON = new JSONObject();
        try {
            mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
            for (String crunchifyKey : JSONObject.getNames(json2)) {
                mergedJSON.put(crunchifyKey, json2.get(crunchifyKey));
            }

        } catch (JSONException e) {
            throw new RuntimeException("JSON Exception" + e);
        }
        return mergedJSON;
    }

    //Send message with pastell
    public static void removeDraftStatus(int msgId, Account account) throws IOException {
        List<Integer> itemIds;
        try {
            Mailbox mbox = MailboxManager.getInstance().getMailboxByAccount(account);
            OperationContext operationContext = new OperationContext(account);
            Message message = mbox.getMessageById(operationContext, msgId);

            message.getUnderlyingData().unsetFlag(Flag.FlagInfo.DRAFT);

            DbPool.DbConnection conn = mbox.getOperationConnection();
            PreparedStatement stmt = null;

            try {
                boolean add = false;
                boolean isFlag = true;
                String primaryUpdate;
                String sanityCheckAnd;
                primaryUpdate = "flags = flags - "+(long)Flag.FlagInfo.DRAFT.toBitmask();
                sanityCheckAnd = "flags <> 0 AND ";

                int count = Math.min(Db.getINClauseBatchSize(), 1);
                stmt = conn.prepareStatement("UPDATE " + DbMailItem.getMailItemTableName(mbox) + " SET " + primaryUpdate + " WHERE " + DbMailItem.IN_THIS_MAILBOX_AND + sanityCheckAnd + "id=" + msgId);
                stmt.executeUpdate();
                stmt.close();
                stmt = null;
            } catch (SQLException var18) {
                throw ServiceException.FAILURE("updating flag data for 1 items: " + msgId, var18);
            } finally {
                DbPool.closeStatement(stmt);
            }
        } catch (Exception e) {
            ZimbraLog.webclient.error("Erreur pastell", e.fillInStackTrace());
        }
    }

}
